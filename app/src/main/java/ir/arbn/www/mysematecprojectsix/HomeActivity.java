package ir.arbn.www.mysematecprojectsix;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView Poster;
    TextView Title, Director, Year, Plot;
    EditText Word;
    Button Search;
    ProgressDialog ProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ProgressDialog = new ProgressDialog(this);
        bindViews();
        Search.setOnClickListener(this);
    }

    void bindViews() {
        Poster = findViewById(R.id.Poster);
        Title = findViewById(R.id.Title);
        Director = findViewById(R.id.Director);
        Year = findViewById(R.id.Year);
        Plot = findViewById(R.id.Plot);
        Word = findViewById(R.id.Word);
        Search = findViewById(R.id.Search);
        ProgressDialog.setTitle("Loading");
        ProgressDialog.setMessage("Please Wait...");
    }

    @Override
    public void onClick(View view) {
        getDataFromOMDB(Word.getText().toString());
    }

    private void getDataFromOMDB(String Word) {
        ProgressDialog.show();
        String URL = "http://www.omdbapi.com/?t=" + Word + "&apikey=383fc2db";
        AsyncHttpClient Client = new AsyncHttpClient();
        Client.get(URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(HomeActivity.this, "Error In Connection!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                GetDataFromJsonString(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                ProgressDialog.dismiss();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    void GetDataFromJsonString(String ServerRespons) {
        Gson Gson = new Gson();
        MovieFinder Model = Gson.fromJson(ServerRespons, MovieFinder.class);
        Glide.with(this).load(Model.getPoster()).into(Poster);
        Title.setText("Title: " + Model.getTitle());
        Director.setText("Director: " + Model.getDirector());
        Year.setText("Year: " + Model.getYear());
        Plot.setText("Description: " + Model.getPlot());
        //Old Model
//        try {
//            JSONObject AllObject = new JSONObject(ServerRespons);
//            String PosterVal = AllObject.getString("Poster");
//            String TitleVal = AllObject.getString("Title");
//            String DirectorVal = AllObject.getString("Director");
//            String YearVal = AllObject.getString("Year");
//            String PlotVal = AllObject.getString("Plot");
//            Glide.with(this).load(PosterVal).into(Poster);
//            Title.setText("Title: " + TitleVal);
//            Director.setText("Director: " + DirectorVal);
//            Year.setText("Year: " + YearVal);
//            Plot.setText("Description: " + PlotVal);
//        } catch (JSONException e) {
//            Toast.makeText(this, "Data Is Wrong!", Toast.LENGTH_SHORT).show();
//        }
    }
}